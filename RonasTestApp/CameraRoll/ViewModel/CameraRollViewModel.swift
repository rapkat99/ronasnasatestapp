//
//  CameraRollViewModel.swift
//  RonasTestApp
//
//  Created by Rapkat on 5/12/22.
//

import Foundation
import Combine

final class CameraRollViewModel: ObservableObject {
    @Published var cameraRollArray: [PhotosModel] = []
    
    private let networkService: NetworkServiceProtocol
    var cameraAbbreviation: String
    var cameraName: String
    var date: String
    
    init(networkService: NetworkServiceProtocol = NetworkService(), cameraAbbreviation: String, cameraName: String, date: String) {
        self.networkService = networkService
        self.cameraAbbreviation = cameraAbbreviation
        self.cameraName = cameraName
        self.date = date
        getPhotos()
    }
    
    func getPhotos() {
        networkService.getMarsPhotos(camera: cameraAbbreviation, date: date) { [weak self] result in
            switch result {
            case .success(let photos):
                guard let self = self else { return }
                self.cameraRollArray = photos.photos
            case .failure(let error):
                print("Error retrieving users: \(error.localizedDescription)")
            }
        }
    }
}
