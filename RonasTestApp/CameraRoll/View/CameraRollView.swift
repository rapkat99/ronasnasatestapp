//
//  CameraRollView.swift
//  RonasTestApp
//
//  Created by Rapkat on 5/12/22.
//

import UIKit
import SnapKit

class CameraRollView: UIView {
    lazy var collectionView: UICollectionView = {
        let layout: UICollectionViewFlowLayout = UICollectionViewFlowLayout()
        layout.sectionInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        layout.scrollDirection = .vertical
        let collectionView = UICollectionView(frame: .zero, collectionViewLayout: layout)
        collectionView.register(CameraRollCell.self, forCellWithReuseIdentifier: CameraRollCell.reuseID)
        collectionView.backgroundColor = .clear
        return collectionView
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupUI()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func setupUI() {
        self.backgroundColor = UIColor(red: 0.86, green: 0.81, blue: 0.75, alpha: 1.00)
        addSubview(collectionView)
        collectionView.snp.makeConstraints { make in
            make.top.equalToSuperview().inset(30)
            make.bottom.equalToSuperview().inset(20)
            make.left.right.equalToSuperview().inset(16)
        }
    }
}
