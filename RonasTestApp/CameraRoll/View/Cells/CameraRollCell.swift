//
//  CameraRollCell.swift
//  RonasTestApp
//
//  Created by Rapkat on 5/12/22.
//

import UIKit
import SDWebImage
import SnapKit

class CameraRollCell: UICollectionViewCell {
    private lazy var imageView: UIImageView = {
       let image = UIImageView()
        image.layer.cornerRadius = 10
        return image
    }()
    
    public static let reuseID = "CameraRollCell"

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override init(frame: CGRect) {
        super.init(frame: frame)
        setupUI()
    }
    
    private func setupUI() {
        self.layer.masksToBounds = true
        self.layer.cornerRadius = 10
        addSubview(imageView)
        imageView.snp.makeConstraints { make in
            make.edges.equalToSuperview()
        }
    }
    
    func setupCell(model: PhotosModel) {
        guard let image = URL(string: model.img_src) else { return }
        imageView.sd_setImage(with: image)
    }
}
