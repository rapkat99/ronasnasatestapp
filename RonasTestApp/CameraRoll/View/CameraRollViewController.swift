//
//  CameraRollViewController.swift
//  RonasTestApp
//
//  Created by Rapkat on 5/12/22.
//

import UIKit
import Combine
import SnapKit

class CameraRollViewController: UIViewController {
    private var viewModel: CameraRollViewModel!
    private var cancellables: Set<AnyCancellable> = []
    
    private lazy var cameraRollView: CameraRollView = {
        let view = CameraRollView(frame: .zero)
        return view
    }()
    
    init(viewModel: CameraRollViewModel) {
        super.init(nibName: nil, bundle: nil)
        self.viewModel = viewModel
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError()
    }
    
    override func viewDidLoad() {
        setupUI()
        viewModel.objectWillChange
            .receive(on: RunLoop.main)
            .sink { [weak self] in
                guard let self = self else { return }
                self.cameraRollView.collectionView.reloadData()
            }.store(in: &cancellables)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        setupNavigationBar()
    }
    
    private func setupUI() {
        view.addSubview(cameraRollView)
        cameraRollView.snp.makeConstraints { make in
            make.edges.equalToSuperview()
        }
        
        cameraRollView.collectionView.dataSource = self
        cameraRollView.collectionView.delegate = self
    }
    
    private func setupNavigationBar() {
        let navigationTitleLabel = UILabel()
        navigationTitleLabel.backgroundColor = UIColor.clear
        navigationTitleLabel.numberOfLines = 2
        navigationTitleLabel.textAlignment = NSTextAlignment.center
        navigationTitleLabel.text = "\(viewModel.cameraName)\n\(viewModel.date)"
        self.navigationItem.titleView = navigationTitleLabel
    }
}

extension CameraRollViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return viewModel.cameraRollArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: CameraRollCell.reuseID, for: indexPath) as! CameraRollCell
        cell.setupCell(model: viewModel.cameraRollArray[indexPath.row])
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width = self.view.frame.width / 3 - 20
        return CGSize(width: width, height: width)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let vm = CameraDetailViewModel(image: viewModel.cameraRollArray[indexPath.row].img_src,
                                       photoId: viewModel.cameraRollArray[indexPath.row].id)
        let vc = CameraDetailViewController(viewModel: vm)
        navigationController?.pushViewController(vc, animated: true)
    }
}

