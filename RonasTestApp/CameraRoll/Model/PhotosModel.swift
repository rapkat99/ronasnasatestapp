//
//  PhotosModel.swift
//  RonasTestApp
//
//  Created by Rapkat on 5/12/22.
//

import Foundation

struct CameraRollModel: Codable {
    let photos: [PhotosModel]
}

struct PhotosModel: Codable {
    let id: Int
    let sol: Int
    let camera: CameraModel
    let img_src: String
    let earth_date: String
    let rover: RoverModel
}


struct CameraModel: Codable {
    let id: Int
    let name: String
    let rover_id: Int
    let full_name: String
}

struct RoverModel: Codable {
    let id: Int
    let name: String
    let landing_date: String
    let launch_date: String
    let status: String
}
