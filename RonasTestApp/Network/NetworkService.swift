//
//  NetworkService.swift
//  RonasTestApp
//
//  Created by Rapkat on 5/12/22.
//

import Foundation

final class NetworkService: NetworkServiceProtocol {
    private let baseUrlString = "https://api.nasa.gov/mars-photos/api/v1/rovers/curiosity/photos"
    private let apiKey = "b19JohffgTEpYoTY9tpcgkbAo87IDLK1OANqJfHE"
    
    private let urlSession: URLSession
    
    init(urlSession: URLSession = URLSession.shared) {
        self.urlSession = urlSession
    }
    
    func getMarsPhotos(camera: String, date: String, completion: @escaping (Result<CameraRollModel, Error>) -> Void) {
        guard var url = URL(string: baseUrlString) else { return }
        let queryItem: [URLQueryItem] = [ URLQueryItem(name: "earth_date", value: date),
                                          URLQueryItem(name: "camera", value: camera),
                                          URLQueryItem(name: "api_key", value: apiKey)
        ]
        url.append(queryItems: queryItem)

        urlSession.dataTask(with: url) { (data, response, error) in
            if let error = error {
                completion(.failure(error))
            }

            do {
                let decoder = JSONDecoder()
                let users = try decoder.decode(CameraRollModel.self, from: data!)
                completion(.success(users))
            } catch let err {
                completion(.failure(err))
            }
        }.resume()
    }
}
