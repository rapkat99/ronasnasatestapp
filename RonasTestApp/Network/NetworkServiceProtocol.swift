//
//  NetworkServiceProtocol.swift
//  RonasTestApp
//
//  Created by Rapkat on 5/12/22.
//

protocol NetworkServiceProtocol {
    func getMarsPhotos(camera: String, date: String, completion: @escaping (Result<CameraRollModel, Error>) -> Void)
}
