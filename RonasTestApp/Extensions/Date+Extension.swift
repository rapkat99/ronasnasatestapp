//
//  Date+Extension.swift
//  RonasTestApp
//
//  Created by Rapkat on 5/12/22.
//

import Foundation

extension Date {
    static let formatterUI: DateFormatter = {
        let formatter = DateFormatter()
        formatter.dateFormat = "dd MMM yyyy"
        return formatter
    }()
    
    static let formatterServer: DateFormatter = {
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd"
        return formatter
    }()
    
    var formattedUI: String {
        return Date.formatterUI.string(from: self)
    }
    
    var formattedServer: String {
        return Date.formatterServer.string(from: self)
    }
}
