//
//  CameraDetailViewModel.swift
//  RonasTestApp
//
//  Created by Rapkat on 5/12/22.
//

import Foundation

class CameraDetailViewModel {
    var image: String
    var photoId: Int
    
    init(image: String, photoId: Int) {
        self.image = image
        self.photoId = photoId
    }
}
