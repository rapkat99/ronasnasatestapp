//
//  CameraDetailView.swift
//  RonasTestApp
//
//  Created by Rapkat on 5/12/22.
//

import UIKit
import SnapKit

class CameraDetailView: UIView {
    lazy var imageView: UIImageView = {
        let image = UIImageView()
        image.layer.cornerRadius = 10
        return image
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupUI()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func setupUI() {
        self.layer.masksToBounds = true
        self.layer.cornerRadius = 10
        
        addSubview(imageView)
        imageView.snp.makeConstraints { make in
            make.bottom.top.equalToSuperview().inset(20)
            make.left.right.equalToSuperview().inset(16)
        }
    }
}
