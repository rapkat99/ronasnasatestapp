//
//  CameraDetailViewController.swift
//  RonasTestApp
//
//  Created by Rapkat on 5/12/22.
//

import UIKit

class CameraDetailViewController: UIViewController {
    private var viewModel: CameraDetailViewModel!
    
    private lazy var cameraDetailView: CameraDetailView = {
        let view = CameraDetailView(frame: .zero)
        return view
    }()
    
    init(viewModel: CameraDetailViewModel) {
        super.init(nibName: nil, bundle: nil)
        self.viewModel = viewModel
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError()
    }
    
    override func viewDidLoad() {
        setupUI()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        setupNavigationBar()
    }
    
    private func setupUI() {
        view.addSubview(cameraDetailView)
        cameraDetailView.snp.makeConstraints { make in
            make.edges.equalTo(view.safeAreaLayoutGuide.snp.edges)
        }
        cameraDetailView.imageView.sd_setImage(with: URL(string: viewModel.image))
    }
    
    private func setupNavigationBar() {
        let navigationTitleLabel = UILabel()
        navigationTitleLabel.backgroundColor = UIColor.clear
        navigationTitleLabel.numberOfLines = 2
        navigationTitleLabel.textAlignment = NSTextAlignment.center
        navigationTitleLabel.textColor = .white
        navigationTitleLabel.text = "Photo ID\n\(viewModel.photoId)"
        navigationItem.titleView = navigationTitleLabel
        navigationItem.rightBarButtonItem = UIBarButtonItem(image: UIImage(named: "share"), style: .plain, target: self, action: nil)
        navigationController?.navigationBar.tintColor = .white
    }
}
