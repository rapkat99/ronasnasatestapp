//
//  SelectionView.swift
//  RonasTestApp
//
//  Created by Rapkat on 5/12/22.
//

import UIKit
import SnapKit

class SelectionView: UIView {    
    lazy var backgroundView: UIView = {
       let view = UIView()
        return view
    }()
    
    lazy var backgroundImage: UIImageView = {
        let image = UIImageView()
        image.image = UIImage(named: "backgroundImage")
        return image
    }()
    
    lazy var mainView: UIView = {
        let view = UIView()
        return view
    }()
    
    lazy var cameraTitleLabel: UILabel = {
        let label = UILabel()
        label.text = "Rover Camera"
        label.font = UIFont.systemFont(ofSize: 14, weight: .regular)
        return label
    }()
    
    lazy var cameraTextView: UIView = {
        let view = UIView()
        view.backgroundColor = .white
        view.layer.cornerRadius = 10
        let gesture = UITapGestureRecognizer(target: self, action:  #selector (self.cameraTextViewAction (_:)))
        view.addGestureRecognizer(gesture)
        return view
    }()

    lazy var cameraTextLabel: UILabel = {
        let label = UILabel()
        label.text = "Front Hazard Avoidance Camera"
        return label
    }()
    
    lazy var arrowDownImage: UIImageView = {
        let image = UIImageView()
        image.image = UIImage(named: "dropdown")
        return image
    }()
    
    lazy var dateTitleLabel: UILabel = {
        let label = UILabel()
        label.text = "Date"
        label.font = UIFont.systemFont(ofSize: 14, weight: .regular)
        return label
    }()
    
    lazy var dateTextView: UIView = {
        let view = UIView()
        view.backgroundColor = .white
        view.layer.cornerRadius = 10
        let gesture = UITapGestureRecognizer(target: self, action:  #selector (self.dateTextViewAction (_:)))
        view.addGestureRecognizer(gesture)
        return view
    }()
    
    lazy var dateTextLabel: UILabel = {
        let label = UILabel()
        label.text = Date().formattedUI
        return label
    }()
    
    lazy var dateImage: UIImageView = {
        let image = UIImageView()
        image.image = UIImage(named: "calendar")
        return image
    }()
    
    lazy var exploreButton: UIButton = {
        let button = UIButton()
        button.backgroundColor = .red
        button.layer.cornerRadius = 10
        button.setTitle("Explore", for: .normal)
        return button
    }()
    
    lazy var toolBar: UIToolbar = {
        var toolBar = UIToolbar()
        toolBar = UIToolbar.init(frame: CGRect.init(x: 0.0, y: UIScreen.main.bounds.size.height - 300, width: UIScreen.main.bounds.size.width, height: 50))
        toolBar.barStyle = .black
        toolBar.items = [UIBarButtonItem.init(title: "Done", style: .done, target: self, action: #selector(toolBarButtonTapped))]
        return toolBar
    }()
    
    lazy var cameraPicker: UIPickerView = {
        let picker = UIPickerView()
        picker.backgroundColor = UIColor.white
        picker.autoresizingMask = .flexibleWidth
        picker.contentMode = .center
        picker.frame = CGRect.init(x: 0.0, y: UIScreen.main.bounds.size.height - 300, width: UIScreen.main.bounds.size.width, height: 300)
        return picker
    }()
    
    lazy var datePicker: UIDatePicker = {
        let picker = UIDatePicker()
        picker.datePickerMode = .date
        picker.preferredDatePickerStyle = .wheels
        picker.backgroundColor = UIColor.white
        picker.autoresizingMask = .flexibleWidth
        picker.contentMode = .center
        picker.frame = CGRect.init(x: 0.0, y: UIScreen.main.bounds.size.height - 300, width: UIScreen.main.bounds.size.width, height: 300)
        picker.addTarget(self, action: #selector(handleDatePicker), for: .valueChanged)
        return picker
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupUI()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func setupUI() {
        addSubview(backgroundView)
        backgroundView.snp.makeConstraints { make in
            make.edges.equalToSuperview()
        }
        
        backgroundView.addSubview(backgroundImage)
        backgroundImage.snp.makeConstraints { make in
            make.edges.equalToSuperview()
        }
        
        addSubview(mainView)
        mainView.snp.makeConstraints { make in
            make.centerY.equalToSuperview()
            make.left.right.equalToSuperview().inset(24)
            make.height.equalTo(285)
        }
        
        mainView.addSubview(cameraTitleLabel)
        cameraTitleLabel.snp.makeConstraints { make in
            make.top.equalToSuperview()
            make.left.right.equalToSuperview()
        }
        
        mainView.addSubview(cameraTextView)
        cameraTextView.snp.makeConstraints { make in
            make.top.equalTo(cameraTitleLabel.snp.bottom).offset(7)
            make.right.left.equalToSuperview()
            make.height.equalTo(60)
        }
        
        cameraTextView.addSubview(arrowDownImage)
        arrowDownImage.snp.makeConstraints { make in
            make.centerY.equalToSuperview()
            make.right.equalToSuperview().inset(16)
            make.height.width.equalTo(24)
        }
        
        cameraTextView.addSubview(cameraTextLabel)
        cameraTextLabel.snp.makeConstraints { make in
            make.centerY.equalToSuperview()
            make.left.equalToSuperview().inset(16)
            make.right.equalTo(arrowDownImage.snp.left)
            make.height.equalTo(23)
        }
        
        mainView.addSubview(dateTitleLabel)
        dateTitleLabel.snp.makeConstraints { make in
            make.top.equalTo(cameraTextView.snp.bottom).offset(16)
            make.left.right.equalToSuperview()
        }
        
        mainView.addSubview(dateTextView)
        dateTextView.snp.makeConstraints { make in
            make.top.equalTo(dateTitleLabel.snp.bottom).offset(7)
            make.right.left.equalToSuperview()
            make.height.equalTo(60)
        }
        
        dateTextView.addSubview(dateTextLabel)
        dateTextLabel.snp.makeConstraints { make in
            make.centerY.equalToSuperview()
            make.left.equalToSuperview().inset(16)
            make.height.equalTo(23)
        }
        
        dateTextView.addSubview(dateImage)
        dateImage.snp.makeConstraints { make in
            make.centerY.equalToSuperview()
            make.right.equalToSuperview().inset(16)
            make.height.width.equalTo(24)
        }
        
        mainView.addSubview(exploreButton)
        exploreButton.snp.makeConstraints { make in
            make.top.equalTo(dateTextView.snp.bottom).offset(40)
            make.left.right.equalToSuperview()
            make.height.equalTo(60)
        }
    }
}

extension SelectionView {
    @objc func cameraTextViewAction(_ sender:UITapGestureRecognizer) {
        self.addSubview(cameraPicker)
        self.addSubview(toolBar)
    }
    
    @objc func dateTextViewAction(_ sender:UITapGestureRecognizer) {
        self.addSubview(datePicker)
        self.addSubview(toolBar)
    }
    
    @objc func handleDatePicker(_ datePicker: UIDatePicker) {
        dateTextLabel.text = datePicker.date.formattedUI
    }
    
    @objc func toolBarButtonTapped() {
        toolBar.removeFromSuperview()
        cameraPicker.removeFromSuperview()
        datePicker.removeFromSuperview()
    }
}
