//
//  ViewController.swift
//  RonasTestApp
//
//  Created by Rapkat on 5/12/22.
//

import UIKit
import SnapKit

class SelectionViewController: UIViewController {
    private var viewModel: SelectionViewModel!
    private lazy var selectionView: SelectionView = {
        let view = SelectionView(frame: .zero)
        return view
    }()
    
    init(viewModel: SelectionViewModel) {
        super.init(nibName: nil, bundle: nil)
        self.viewModel = viewModel
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
    }
    
    private func setupUI() {
        view.addSubview(selectionView)
        selectionView.snp.makeConstraints { make in
            make.edges.equalToSuperview()
        }
        navigationItem.title = "Select Camera and Date"
        
        selectionView.cameraPicker.delegate = self
        selectionView.cameraPicker.dataSource = self
        
        selectionView.exploreButton.addTarget(self, action: #selector(buttonAction), for: .touchUpInside)
    }
    
    @objc func buttonAction(sender: UIButton!) {
        guard let cameraName = selectionView.cameraTextLabel.text else { return }
        let vm = CameraRollViewModel(cameraAbbreviation: viewModel.getMarsCamerasAbbreviation(marsCameras: MarsCameras(rawValue: cameraName)),
                                     cameraName: cameraName,
                                     date: selectionView.datePicker.date.formattedServer)
        let vc = CameraRollViewController(viewModel: vm)
        navigationController?.pushViewController(vc, animated: true)
    }
}

extension SelectionViewController: UIPickerViewDelegate, UIPickerViewDataSource{
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return viewModel.array.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return viewModel.array[row]
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        selectionView.cameraTextLabel.text = viewModel.array[row].description
    }
}
