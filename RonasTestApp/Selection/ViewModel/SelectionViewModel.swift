//
//  SelectionViewModel.swift
//  RonasTestApp
//
//  Created by Rapkat on 5/12/22.
//


import Foundation
enum MarsCameras: String {
    case FHAZ = "Front Hazard Avoidance Camera"
    case RHAZ = "Rear Hazard Avoidance Camera"
    case MAST = "Mast Camera"
    case CHEMCAM = "Chemistry and Camera Complex"
    case MAHLI = "Mars Hand Lens Imager"
    case MARDI = "Mars Descent Imager"
    case NAVCAM = "Navigation Camera"
    case PANCAM = "Panoramic Camera"
    case MINITES = "Miniature Thermal Emission Spectrometer (Mini-TES)"
}

class SelectionViewModel {
    
    let array = ["Front Hazard Avoidance Camera", "Rear Hazard Avoidance Camera", "Mast Camera", "Chemistry and Camera Complex", "Mars Hand Lens Imager",
                 "Mars Descent Imager", "Navigation Camera", "Panoramic Camera", "Miniature Thermal Emission Spectrometer (Mini-TES)"]
    
    func getMarsCamerasAbbreviation(marsCameras: MarsCameras?) -> String {
        switch marsCameras {
        case .FHAZ:
            return String("\(MarsCameras.FHAZ)")
        case .RHAZ:
            return String("\(MarsCameras.RHAZ)")
        case .MAST:
            return String("\(MarsCameras.MAST)")
        case .CHEMCAM:
            return String("\(MarsCameras.CHEMCAM)")
        case .MAHLI:
            return String("\(MarsCameras.MAHLI)")
        case .MARDI:
            return String("\(MarsCameras.MARDI)")
        case .NAVCAM:
            return String("\(MarsCameras.NAVCAM)")
        case .PANCAM:
            return String("\(MarsCameras.PANCAM)")
        case .MINITES:
            return String("\(MarsCameras.MINITES)")
        case .none:
            return "error"
        }
    }
}
